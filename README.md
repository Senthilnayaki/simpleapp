## Tech Stack
  Python 
  Flask
  - Gitlab
  - Jenkins(with required plugin gitlab,gitlab api,credentials api)
  - ngork

Test the Gitlab connection from Jenkins before starting

## Stages
  - Initializing repository
  - Pushing the code files
  - Tunneling IP
  - Configuring WebHook
  - Setting up gitlab pipeline and jenkinsfile

## Creating Public IP
Since webhook requires a public IP, and jenkins runs in the local machine, you can change the domain for jenkins, if you have one, else

Install ngrok
Sign up and get the auth token, run it in the terminal
run bash ngrok http http://localhost:8080 in terminal
This will provide you a public domain, we will use this to create webhook

## Jenkins reconfigure
Navigate to Manage jenkins -> System -> Jenkins location.
Change the jenkins URL to the public domain from ngrok.
Create a pipeline.

## Webhook Configuration

 **Jenkins**
Under Build triggers -> Enable Build when a change is pushed to GitLab.
Under Advanced, generate the secret token.


**GitLab**
Navigate to repository settings -> webhook -> Add new webhook.
Paste the URL and Secret token from jenkins.
Enable push events

## Setting up Gitlab pipeline and jenkins pipeline

write the pipeline script for jenkins and name it as jenkins it should contain stages such as
  **Checkout**
   **Build** 
  **Deploy**

   **Checkout** 
   Checkout the main branch from GitLab repository
  
  **Build**
    In build stage use docker image python 3.9-slim image to build the code
  
   **Deploy**
    In deploy install all the dependencies such as flask and compile python files

**Gitlab**
  Write a script to trigger jenkins pipeline by using triggers in .gitlab-ci.yml file.




